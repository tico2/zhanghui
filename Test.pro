QT       += core gui
QT       += network
QT       += multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    connections/blockinghttpconnection.cpp \
    connections/networkconnection.cpp \
    connections/noneblockinghttpconnection.cpp \
    connections/noneblockingnetworkconnection.cpp \
    connections/tcpconnection.cpp \
    connections/telnetconnection.cpp \
    devices/audio.cpp \
    devices/biamp.cpp \
    devices/camera.cpp \
    devices/camera/CameraPanasonic.cpp \
    devices/crestron.cpp \
    devices/device.cpp \
    devices/devicemanager.cpp \
    devices/matrix.cpp \
    devices/maxhub.cpp \
    devices/toggle.cpp \
    main.cpp \
    mainwindow.cpp \
    multiwidgets/multibutton.cpp \
    multiwidgets/multibuttongroup.cpp \
    multiwidgets/multilineedit.cpp \
    multiwidgets/multiprogressbar.cpp \
    multiwidgets/multislider.cpp \
    projectdevice.cpp \
    widgets/meterbar.cpp \
    widgets/videoframe.cpp

HEADERS += \
    connections/blockinghttpconnection.h \
    connections/networkconnection.h \
    connections/noneblockinghttpconnection.h \
    connections/noneblockingnetworkconnection.h \
    connections/tcpconnection.h \
    connections/telnetconnection.h \
    devices/audio.h \
    devices/biamp.h \
    devices/camera.h \
    devices/camera/CameraPanasonic.h \
    devices/crestron.h \
    devices/device.h \
    devices/devicemanager.h \
    devices/matrix.h \
    devices/maxhub.h \
    devices/toggle.h \
    mainwindow.h \
    multiwidgets/multibutton.h \
    multiwidgets/multibuttongroup.h \
    multiwidgets/multilineedit.h \
    multiwidgets/multiprogressbar.h \
    multiwidgets/multislider.h \
    projectdevice.h \
    widgets/meterbar.h \
    widgets/videoframe.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


CONFIG(debug,debug|release) {
LIBS       += -LD:\VLC-QT\lib -lVLCQtCored -lVLCQtWidgetsd
} else {
LIBS       += -LD:\VLC-QT\lib -lVLCQtCore -lVLCQtWidgets
}
INCLUDEPATH += D:\VLC-QT\include

RESOURCES += \
    res.qrc
