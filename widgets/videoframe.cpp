﻿#include "videoframe.h"
#include "devices/matrix.h"

#include <VLCQtCore/Audio.h>
#include <QDrag>
#include <QPainter>
#include <QMimeData>

VideoFrame::VideoFrame(QWidget *parent) : QFrame(parent) {
    setAcceptDrops(true);
    instance = new VlcInstance(VlcCommon::args(), this);
    player = new VlcMediaPlayer(instance);

    offLineLabel = new QLabel(this);
    const QPixmap offLinePixMap(":/icons/resources/video_off_line_icon.png");
    offLineLabel->setFixedSize(offLinePixMap.width(), offLinePixMap.height());
    offLineLabel->setPixmap(offLinePixMap);
//    offLineLabel->hide();

    noSignalLabel = new QLabel(this);
    const QPixmap noSignalPixMap(":/icons/resources/video_no_signal_icon.png");
    noSignalLabel->setFixedSize(noSignalPixMap.width(), noSignalPixMap.height());
    noSignalLabel->setPixmap(noSignalPixMap);
    noSignalLabel->hide();

    widgetVideo = new VlcWidgetVideo(player, this);
    player->setVideoWidget(widgetVideo);
    widgetVideo->hide();

    QObject::connect(player, QOverload<int>::of(&VlcMediaPlayer::buffering), this, [=](int count) {
        switch (count) {
        case 100:
            widgetVideo->setVisible(true);
            offLineLabel->setVisible(false);
            noSignalLabel->setVisible(false);
            break;
        }
    });
    QObject::connect(player, &VlcMediaPlayer::error, this, [=] {
        // 离线
        widgetVideo->setVisible(false);
        offLineLabel->setVisible(true);
        noSignalLabel->setVisible(false);
        if (!stoppedFlag) {
            play();
        }
    });
    QObject::connect(player, &VlcMediaPlayer::end, this, [=] {
        // 无信号
        widgetVideo->setVisible(false);
        offLineLabel->setVisible(false);
        noSignalLabel->setVisible(true);
        if (!stoppedFlag) {
            play();
        }
    });
}

void VideoFrame::setInfo(VideoMatrix* matrix, const QString &videoName, int channel, const QString &url) {
    this->matrix = matrix;
    this->name = videoName;
    this->channel = channel;
    setUrl(url);
}

void VideoFrame::setUrl(const QString& url) {
    this->url = url;
    media = new VlcMedia(url, instance);
}

void VideoFrame::play() {
    if (media != nullptr) {
        player->stop();
        stoppedFlag = false;
        player->open(media);
        player->audio()->setMute(true);
    }
}

void VideoFrame::mouseDoubleClickEvent(QMouseEvent*) {
    player->stop();
    player->play();
}

void VideoFrame::showEvent(QShowEvent *event) {
    qDebug("showEvent: %d", event->type());
}

void VideoFrame::resizeEvent(QResizeEvent *) {
    noSignalLabel->setGeometry((this->width() - noSignalLabel->width()) / 2, (this->height() - noSignalLabel->height()) / 2, noSignalLabel->width(), noSignalLabel->height());
    offLineLabel->setGeometry((this->width() - offLineLabel->width()) / 2, (this->height() - offLineLabel->height()) / 2, offLineLabel->width(), offLineLabel->height());
    widgetVideo->setGeometry(0, 0, this->width(), this->height());
}

InputVideoFrame::InputVideoFrame(QWidget* parent) : VideoFrame(parent) {}

void InputVideoFrame::mousePressEvent(QMouseEvent *event) {
    if (event->button() != Qt::LeftButton || matrix == nullptr) {
        return;
    }
    QDrag* drag = new QDrag(this);
    QMimeData* mimeData = new QMimeData();
    mimeData->setData("matrix_id", matrix->id.toUtf8());
    mimeData->setData("matrix_channel", QString::number(channel).toUtf8());
    mimeData->setData("matrix_video_url", url.toUtf8());
    drag->setMimeData(mimeData);

    QRect r(0, 0, 160, 90);
    QPixmap pix(160, 90);
    QPainter textPainter(&pix);
    textPainter.fillRect(r, QBrush(QColor(130,200,240)));

    textPainter.setPen(Qt::white);
    textPainter.setFont(QFont("PingFang SC", 25, 40));
    textPainter.drawText(r, name, Qt::AlignHCenter | Qt::AlignVCenter);

    drag->setPixmap(pix);
    drag->exec(Qt::MoveAction);
}

OutputVideoFrame::OutputVideoFrame(QWidget* parent) : VideoFrame(parent) {}

void OutputVideoFrame::setInfo(VideoMatrix *matrix, const QString &videoName, int channel) {
    VideoFrame::setInfo(matrix, videoName, channel, "");
    noVideoUrl = true;
}

void OutputVideoFrame::dragEnterEvent(QDragEnterEvent *event) {
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasFormat("matrix_id") && mimeData->hasFormat("matrix_channel")) {
        event->acceptProposedAction();
    }
    setCursor(Qt::OpenHandCursor);
}

void OutputVideoFrame::dropEvent(QDropEvent *event) {
    const QMimeData* mimeData = event->mimeData();
    const QString inputMatrixId = mimeData->data("matrix_id");
    const int inputChannel = mimeData->data("matrix_channel").toInt();
    if (inputMatrixId == matrix->id) {
        matrix->switchTo(inputChannel, channel);
        if (noVideoUrl) {
            setUrl(mimeData->data("matrix_video_url"));
            play();
        }
    }
}

