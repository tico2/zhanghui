﻿#include "meterbar.h"
#include <QPainter>

MeterBar::MeterBar(QWidget *parent) : QProgressBar (parent) {}

void MeterBar::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);
    int val = value();

    int w = width();
    int h = height();

    float l1 = 24;
    float l2 = 0;
    float min = minimum();
    float max = maximum();
    float total = max - min;

    int redHeight = (max - l1) / total * h;
    int yellowHeight = (l1 - l2) / total * h;
    int greenHeight = (l2 - min) / total * h;

    QBrush b1(QColor(255, 0, 0, 20));
    QBrush b2(QColor(255, 255, 0, 20));
    QBrush b3(QColor(0, 255, 0, 20));

    QBrush f1(QColor(255, 0, 0, 255));
    QBrush f2(QColor(255, 255, 0, 255));
    QBrush f3(QColor(0, 255, 0, 255));

    if (val < l2) {
        int h = (val - min) / (l2 - min) * greenHeight;
        painter.setBrush(b1);
        painter.drawRect(QRect(0, 0, w, redHeight));
        painter.setBrush(b2);
        painter.drawRect(QRect(0, redHeight, w, yellowHeight));
        painter.setBrush(b3);
        painter.drawRect(QRect(0, redHeight + yellowHeight, w, greenHeight));
        painter.setBrush(f3);
        painter.drawRect(QRect(0, redHeight + yellowHeight + greenHeight - h, w, h));
    } else if (val >= l1) {
        int h = (val - l1) / (max - l1) * redHeight;
        painter.setBrush(b1);
        painter.drawRect(QRect(0, 0, w, redHeight));
        painter.setBrush(f1);
        painter.drawRect(QRect(0, redHeight - h, w, h));
        painter.setBrush(f2);
        painter.drawRect(QRect(0, redHeight, w, yellowHeight));
        painter.setBrush(f3);
        painter.drawRect(QRect(0, redHeight + yellowHeight, w, greenHeight));
    } else {
        int h = (val - l2) / (l1 - l2) * yellowHeight;
        painter.setBrush(b1);
        painter.drawRect(QRect(0, 0, w, redHeight));
        painter.setBrush(b2);
        painter.drawRect(QRect(0, redHeight, w, yellowHeight));
        painter.setBrush(f2);
        painter.drawRect(QRect(0, redHeight + yellowHeight - h, w, h));
        painter.setBrush(f3);
        painter.drawRect(QRect(0, redHeight + yellowHeight, w, greenHeight));
    }
}
