﻿#ifndef METERBAR_H
#define METERBAR_H

#include <QProgressBar>

class MeterBar : public QProgressBar
{
    Q_OBJECT
public:
    explicit MeterBar(QWidget *parent = nullptr);
private:
    void paintEvent(QPaintEvent*) override;
};

#endif // METERBAR_H
