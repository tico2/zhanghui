﻿#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <QFrame>
#include <QLabel>
#include <QVideoWidget>
#include <QMediaPlayer>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>
#include <VLCQtWidgets/WidgetVideo.h>
#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>
#include <QShowEvent>

class VideoMatrix;

class VideoFrame : public QFrame
{
    Q_OBJECT
public:
    explicit VideoFrame(QWidget *parent = nullptr);

    void setInfo(VideoMatrix* matrix, const QString& videoName, int channel, const QString& url);
    void setUrl(const QString& url);
    void play();

    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void showEvent(QShowEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

protected:
    VideoMatrix* matrix = nullptr;
    QString name;
    int channel;
    QString url;
    bool stoppedFlag = true;
private:
    VlcInstance* instance;
    VlcMediaPlayer* player;
    VlcMedia* media = nullptr;

    QLabel* offLineLabel;
    QLabel* noSignalLabel;
    VlcWidgetVideo* widgetVideo;
};

class InputVideoFrame : public VideoFrame {
    Q_OBJECT
public:
    explicit InputVideoFrame(QWidget *parent = nullptr);
protected:
    void mousePressEvent(QMouseEvent *event) override;
};

class OutputVideoFrame : public VideoFrame {
    Q_OBJECT
public:
    explicit OutputVideoFrame(QWidget *parent = nullptr);

    void setInfo(VideoMatrix* matrix, const QString& videoName, int channel);
protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;

    bool noVideoUrl = false;
};

#endif // VIDEOFRAME_H
