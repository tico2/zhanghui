#ifndef NONEBLOCKINGHTTPCONNECTION_H
#define NONEBLOCKINGHTTPCONNECTION_H

#include "noneblockingnetworkconnection.h"

class NoneBlockingHttpConnection : public NoneBlockingNetworkConnection {
    Q_OBJECT
public:
    explicit NoneBlockingHttpConnection();
    virtual void get(QUrl url);
    virtual void post(QUrl url, QByteArray data);
protected:
    virtual void connectToHostImpl() override;
    virtual void disconnectFromHostImpl() override;
private:
    QNetworkAccessManager qnam;
    bool connectedFlag = false;
};

#endif // NONEBLOCKINGHTTPCONNECTION_H
