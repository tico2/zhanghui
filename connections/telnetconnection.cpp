﻿#include "telnetconnection.h"

const QString TelnetConnection::kOptionString[256] = {
    "BINARY", "ECHO", "RCP", "SUPPRESS GO AHEAD", "NAME", "STATUS",
    "TIMING MARK", "RCTE", "NAOL", "NAOP", "NAOCRD", "NAOHTS", "NAOHTD",
    "NAOFFD", "NAOVTS", "NAOVTD", "NAOLFD", "EXTEND ASCII", "LOGOUT",
    "BYTE MACRO", "DATA ENTRY TERMINAL", "SUPDUP", "SUPDUP OUTPUT",
    "SEND LOCATION", "TERMINAL TYPE", "END OF RECORD", "TACACS UID",
    "OUTPUT MARKING", "TTYLOC", "3270 REGIME", "X.3 PAD", "NAWS", "TSPEED",
    "LFLOW", "LINEMODE", "XDISPLOC", "OLD-ENVIRON", "AUTHENTICATION",
    "ENCRYPT", "NEW-ENVIRON", "TN3270E", "XAUTH", "CHARSET", "RSP",
    "Com Port Control", "Suppress Local Echo", "Start TLS",
    "KERMIT", "SEND-URL", "FORWARD_X", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "TELOPT PRAGMA LOGON", "TELOPT SSPI LOGON",
    "TELOPT PRAGMA HEARTBEAT", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "Extended-Options-List"
};
const unsigned char TelnetConnection::kMaxOptionValue = 255;
const unsigned char TelnetConnection::BINARY = 0;
const unsigned char TelnetConnection::ECHO = 1;
const unsigned char TelnetConnection::PREPARE_TO_RECONNECT = 2;
const unsigned char TelnetConnection::SUPPRESS_GO_AHEAD = 3;
const unsigned char TelnetConnection::APPROXIMATE_MESSAGE_SIZE = 4;
const unsigned char TelnetConnection::STATUS = 5;
const unsigned char TelnetConnection::TIMING_MARK = 6;
const unsigned char TelnetConnection::REMOTE_CONTROLLED_TRANSMISSION = 7;
const unsigned char TelnetConnection::NEGOTIATE_OUTPUT_LINE_WIDTH = 8;
const unsigned char TelnetConnection::NEGOTIATE_OUTPUT_PAGE_SIZE = 9;
const unsigned char TelnetConnection::NEGOTIATE_CARRIAGE_RETURN = 10;
const unsigned char TelnetConnection::NEGOTIATE_HORIZONTAL_TAB_STOP = 11;
const unsigned char TelnetConnection::NEGOTIATE_HORIZONTAL_TAB = 12;
const unsigned char TelnetConnection::NEGOTIATE_FORMFEED = 13;
const unsigned char TelnetConnection::NEGOTIATE_VERTICAL_TAB_STOP = 14;
const unsigned char TelnetConnection::NEGOTIATE_VERTICAL_TAB = 15;
const unsigned char TelnetConnection::NEGOTIATE_LINEFEED = 16;
const unsigned char TelnetConnection::EXTENDED_ASCII = 17;
const unsigned char TelnetConnection::FORCE_LOGOUT = 18;
const unsigned char TelnetConnection::BYTE_MACRO = 19;
const unsigned char TelnetConnection::DATA_ENTRY_TERMINAL = 20;
const unsigned char TelnetConnection::SUPDUP = 21;
const unsigned char TelnetConnection::SUPDUP_OUTPUT = 22;
const unsigned char TelnetConnection::SEND_LOCATION = 23;
const unsigned char TelnetConnection::TERMINAL_TYPE = 24;
const unsigned char TelnetConnection::END_OF_RECORD = 25;
const unsigned char TelnetConnection::TACACS_USER_IDENTIFICATION = 26;
const unsigned char TelnetConnection::OUTPUT_MARKING = 27;
const unsigned char TelnetConnection::TERMINAL_LOCATION_NUMBER = 28;
const unsigned char TelnetConnection::REGIME_3270 = 29;
const unsigned char TelnetConnection::X3_PAD = 30;
const unsigned char TelnetConnection::WINDOW_SIZE = 31;
const unsigned char TelnetConnection::TERMINAL_SPEED = 32;
const unsigned char TelnetConnection::REMOTE_FLOW_CONTROL = 33;
const unsigned char TelnetConnection::LINEMODE = 34;
const unsigned char TelnetConnection::X_DISPLAY_LOCATION = 35;
const unsigned char TelnetConnection::OLD_ENVIRONMENT_VARIABLES = 36;
const unsigned char TelnetConnection::AUTHENTICATION = 37;
const unsigned char TelnetConnection::ENCRYPTION = 38;
const unsigned char TelnetConnection::NEW_ENVIRONMENT_VARIABLES = 39;
const unsigned char TelnetConnection::EXTENDED_OPTIONS_LIST = 255;


const QString TelnetConnection::kCommandString[20] = {
    "IAC", "DONT", "DO", "WONT", "WILL", "SB", "GA", "EL", "EC", "AYT",
    "AO", "IP", "BRK", "DMARK", "NOP", "SE", "EOR", "ABORT", "SUSP", "EOF"
};
const unsigned char TelnetConnection::kMaxCommandValue = 255;
const unsigned char TelnetConnection::IAC = 255;
const unsigned char TelnetConnection::DONT = 254;
const unsigned char TelnetConnection::DO = 253;
const unsigned char TelnetConnection::WONT = 252;
const unsigned char TelnetConnection::WILL = 251;
const unsigned char TelnetConnection::SB = 250;
const unsigned char TelnetConnection::GA = 249;
const unsigned char TelnetConnection::EL = 248;
const unsigned char TelnetConnection::EC = 247;
const unsigned char TelnetConnection::AYT = 246;
const unsigned char TelnetConnection::AO = 245;
const unsigned char TelnetConnection::IP = 244;
const unsigned char TelnetConnection::BREAK = 243;
const unsigned char TelnetConnection::DM = 242;
const unsigned char TelnetConnection::NOP = 241;
const unsigned char TelnetConnection::SE = 240;
const unsigned char TelnetConnection::EOR = 239;
const unsigned char TelnetConnection::ABORT = 238;
const unsigned char TelnetConnection::SUSP = 237;
const unsigned char TelnetConnection::EOFF = 236;

TelnetConnection::TelnetConnection(const QString& hostname, unsigned short port) : TcpConnection (hostname, port) {
    QObject::disconnect(client, &QTcpSocket::connected, this, &TcpConnection::connected);
}

void TelnetConnection::processMessage() {
    QByteArray data = client->readAll();
//    qDebug() << "Receive:" << data;
    if (static_cast<unsigned char>(data[0]) == IAC) {
//        QString readable = QString("");
        for (int i = 0; i < data.length(); i++) {
            if (static_cast<unsigned char>(data[i]) == IAC) {
                if (i != 0) {
//                    qDebug() << "Control msg:" << (readable);
//                    readable.clear();
                }
                i++;
//                readable.append("IAC ").append(commandString[MAX_COMMAND_VALUE - static_cast<unsigned char>(data[i])]);
                switch (static_cast<unsigned char>(data[i])) {
                case DO:
                    data[i] = static_cast<char>(WONT);
                    break;
                case WILL:
                    data[i] = static_cast<char>(DONT);
                    break;
                }
//            } else {
//                readable.append(' ').append(optionString[static_cast<unsigned char>(data[i])]);
            }
        }
//            qDebug() << "Control msg:" << (readable);
        client->write(data);
        client->flush();
    } else {
        if (!isConnected()) {
            emit connected();
        }
        for (int i = 0; i < data.length(); i++) {
            switch (data[i]) {
            case '\n':
                emit receiveMessage(unfinishedData);
                unfinishedData.clear();
                break;
            case '\r':
                break;
            default:
                unfinishedData.append(data[i]);
            }
        }
    }
}
