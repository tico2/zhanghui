﻿#include "tcpconnection.h"

TcpConnection::TcpConnection(const QString& hostname, unsigned short port) {
    this->host = hostname;
    this->port = port;
    QObject::connect(client, &QTcpSocket::connected, this, &TcpConnection::connected);
    QObject::connect(client, &QTcpSocket::disconnected, this, &TcpConnection::disconnected);
    QObject::connect(client, &QTcpSocket::readyRead, this, &TcpConnection::processMessage);
    //TODO
//    QObject::connect(client, &QTcpSocket::error, this, &TcpConnection::receiveError);
}

TcpConnection::~TcpConnection() {
    delete client;
}

void TcpConnection::processMessage() {
    QByteArray data = client->readAll();
    QByteArray msgData;
    for (int i = 0; i < data.size(); i++) {
        if (data[i] == splitFlag) {
            emit receiveMessage(msgData);
            msgData.clear();
        } else {
            msgData.append(data[i]);
        }
    }
}

void TcpConnection::write(QByteArray data) {
    data.append(splitFlag);
    client->write(data);
    client->flush();
}

void TcpConnection::connectToHostImpl() {
    qDebug("Connecting: %s:%d", qPrintable(host), port);
    client->abort();
    client->connectToHost(host, port);
}

void TcpConnection::disconnectFromHostImpl() {
    client->disconnectFromHost();
}
