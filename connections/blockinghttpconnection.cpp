﻿#include "blockinghttpconnection.h"

BlockingHttpConnection::BlockingHttpConnection() {}

int BlockingHttpConnection::get(QUrl url, QString* response) {
    return getFromReply(qnam.get(QNetworkRequest(url)), response);
}

int BlockingHttpConnection::post(QUrl url, QByteArray data, QString* response) {
    return getFromReply(qnam.post(QNetworkRequest(url), data), response);
}

int BlockingHttpConnection::getFromReply(QNetworkReply* reply, QString* response) {
    QEventLoop eventLoop;
    QTimer overtime;
    QObject::connect(&overtime, &QTimer::timeout, &eventLoop, &QEventLoop::quit);
    QObject::connect(reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
    overtime.start(2000);
    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);
    // 连接超时
    if (!overtime.isActive()) {
        reply->abort();
        return -1;
    }
    // 未连接超时
    overtime.stop();
    if (response != nullptr) {
        if (reply->error()) {
            *response = reply->errorString();
        } else {
            *response = reply->readAll();
        }
    }
    return reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
}

void BlockingHttpConnection::connectToHostImpl() {
    emit connected();
}

void BlockingHttpConnection::disconnectFromHostImpl() {
    emit disconnected();
}
