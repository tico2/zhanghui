#ifndef CONNECTION_H
#define CONNECTION_H

#include "noneblockingnetworkconnection.h"

class BlockingHttpConnection : public NetworkConnection {
    Q_OBJECT
public:
    explicit BlockingHttpConnection();
    int get(QUrl url, QString* response = nullptr);
    int post(QUrl url, QByteArray data, QString* response = nullptr);
protected:
    virtual void connectToHostImpl() override;
    virtual void disconnectFromHostImpl() override;
private:
    QNetworkAccessManager qnam;
    bool connectedFlag = false;
    int getFromReply(QNetworkReply* reply, QString* response = nullptr);
};

#endif // CONNECTION_H
