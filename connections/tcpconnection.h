﻿#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include "noneblockingnetworkconnection.h"

class TcpConnection : public NoneBlockingNetworkConnection {
    Q_OBJECT
public:
    explicit TcpConnection(const QString& hostname, unsigned short port);
    virtual ~TcpConnection() override;
    void setSplitFlag(char c);
    void write(QByteArray data);
protected:
    virtual void connectToHostImpl() override;
    virtual void disconnectFromHostImpl() override;
    virtual void processMessage();
    QTcpSocket* client = new QTcpSocket();
private:
    char splitFlag = '\n';
    QString host;
    unsigned short port;
};

#endif // TCPCONNECTION_H
