﻿#ifndef TELNETCONNECTION_H
#define TELNETCONNECTION_H

#include "tcpconnection.h"

class TelnetConnection : public TcpConnection {
    Q_OBJECT
public:
    explicit TelnetConnection(const QString& hostname, unsigned short port = 23);
protected:
    virtual void processMessage() override;
private:
    QByteArray unfinishedData;

    static const QString kOptionString[256];
    static const unsigned char kMaxOptionValue;
    static const unsigned char BINARY;
    static const unsigned char ECHO;
    static const unsigned char PREPARE_TO_RECONNECT;
    static const unsigned char SUPPRESS_GO_AHEAD;
    static const unsigned char APPROXIMATE_MESSAGE_SIZE;
    static const unsigned char STATUS;
    static const unsigned char TIMING_MARK;
    static const unsigned char REMOTE_CONTROLLED_TRANSMISSION;
    static const unsigned char NEGOTIATE_OUTPUT_LINE_WIDTH;
    static const unsigned char NEGOTIATE_OUTPUT_PAGE_SIZE;
    static const unsigned char NEGOTIATE_CARRIAGE_RETURN;
    static const unsigned char NEGOTIATE_HORIZONTAL_TAB_STOP;
    static const unsigned char NEGOTIATE_HORIZONTAL_TAB;
    static const unsigned char NEGOTIATE_FORMFEED;
    static const unsigned char NEGOTIATE_VERTICAL_TAB_STOP;
    static const unsigned char NEGOTIATE_VERTICAL_TAB;
    static const unsigned char NEGOTIATE_LINEFEED;
    static const unsigned char EXTENDED_ASCII;
    static const unsigned char FORCE_LOGOUT;
    static const unsigned char BYTE_MACRO;
    static const unsigned char DATA_ENTRY_TERMINAL;
    static const unsigned char SUPDUP;
    static const unsigned char SUPDUP_OUTPUT;
    static const unsigned char SEND_LOCATION;
    static const unsigned char TERMINAL_TYPE;
    static const unsigned char END_OF_RECORD;
    static const unsigned char TACACS_USER_IDENTIFICATION;
    static const unsigned char OUTPUT_MARKING;
    static const unsigned char TERMINAL_LOCATION_NUMBER;
    static const unsigned char REGIME_3270;
    static const unsigned char X3_PAD;
    static const unsigned char WINDOW_SIZE;
    static const unsigned char TERMINAL_SPEED;
    static const unsigned char REMOTE_FLOW_CONTROL;
    static const unsigned char LINEMODE;
    static const unsigned char X_DISPLAY_LOCATION;
    static const unsigned char OLD_ENVIRONMENT_VARIABLES;
    static const unsigned char AUTHENTICATION;
    static const unsigned char ENCRYPTION;
    static const unsigned char NEW_ENVIRONMENT_VARIABLES;
    static const unsigned char EXTENDED_OPTIONS_LIST;


    static const QString kCommandString[20];
    static const unsigned char kMaxCommandValue;
    static const unsigned char IAC;
    static const unsigned char DONT;
    static const unsigned char DO;
    static const unsigned char WONT;
    static const unsigned char WILL;
    static const unsigned char SB;
    static const unsigned char GA;
    static const unsigned char EL;
    static const unsigned char EC;
    static const unsigned char AYT;
    static const unsigned char AO;
    static const unsigned char IP;
    static const unsigned char BREAK;
    static const unsigned char DM;
    static const unsigned char NOP;
    static const unsigned char SE;
    static const unsigned char EOR;
    static const unsigned char ABORT;
    static const unsigned char SUSP;
    static const unsigned char EOFF;
};

#endif // TELNETCONNECTION_H
