﻿#ifndef NETWORKCONNECTION_H
#define NETWORKCONNECTION_H

#include <QObject>
#include <QtNetwork>
#include <QTimer>

class NetworkConnection : public QObject {
    Q_OBJECT
public:
    explicit NetworkConnection();
    void connectToHost();
    void disconnectFromHost();
    void setConnectTimeout(int timeout);
    bool isConnected();
signals:
    void connected();
    void disconnected();
protected:
    virtual void connectToHostImpl() = 0;
    virtual void disconnectFromHostImpl() = 0;
private:
    QTimer timer;
    int connectionTimeout = 5000;
    bool connectedFlag = false;
};

#endif // NETWORKCONNECTION_H
