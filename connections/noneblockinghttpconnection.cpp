#include "noneblockinghttpconnection.h"

NoneBlockingHttpConnection::NoneBlockingHttpConnection() {
    QObject::connect(&qnam, &QNetworkAccessManager::finished, this, [=](QNetworkReply* reply) {
        int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (statusCode == 200) {
            receiveMessage(QString::fromUtf8(reply->readAll()));
        } else {
            qWarning() << "HTTP ERROR " + QString::number(statusCode);
        }
    });
}

void NoneBlockingHttpConnection::get(QUrl url) {
    qnam.get(QNetworkRequest(url));
}

void NoneBlockingHttpConnection::post(QUrl url, QByteArray data) {
    qnam.post(QNetworkRequest(url), data);
}

void NoneBlockingHttpConnection::connectToHostImpl() {
    emit connected();
}

void NoneBlockingHttpConnection::disconnectFromHostImpl() {
    emit disconnected();
}
