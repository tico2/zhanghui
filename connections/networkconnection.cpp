﻿#include "networkconnection.h"

NetworkConnection::NetworkConnection() {
    QObject::connect(&timer, &QTimer::timeout, this, &NetworkConnection::connectToHostImpl);
    QObject::connect(this, &NetworkConnection::connected, this, [=]() {
        timer.stop();
        connectedFlag = true;
    });
    QObject::connect(this, &NetworkConnection::disconnected, this, [=]() {
        timer.start(connectionTimeout);
        connectedFlag = false;
    });
}

void NetworkConnection::connectToHost() {
    connectToHostImpl();
    timer.start(connectionTimeout);
}

void NetworkConnection::disconnectFromHost() {
    disconnectFromHostImpl();
    timer.stop();
}

void NetworkConnection::setConnectTimeout(int connectTimeout) {
    this->connectionTimeout = connectTimeout;
}

bool NetworkConnection::isConnected() {
    return connectedFlag;
}
