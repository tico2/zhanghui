﻿#ifndef NONEBLOCKINGNETWORKCONNECTION_H
#define NONEBLOCKINGNETWORKCONNECTION_H

#include "networkconnection.h"

class NoneBlockingNetworkConnection : public NetworkConnection {
    Q_OBJECT
public:
    explicit NoneBlockingNetworkConnection();
signals:
    void receiveMessage(const QString& msg);
    void receiveError(QAbstractSocket::SocketError err);
};
#endif // NONEBLOCKINGNETWORKCONNECTION_H
