﻿#ifndef MULTIPROGRESSBAR_H
#define MULTIPROGRESSBAR_H

#include <QObject>
#include <QSet>
#include <QProgressBar>

class MultiProgressBar : public QObject
{
    Q_OBJECT
public:
    explicit MultiProgressBar(int min, int max, QProgressBar* progressBar, QObject *parent = nullptr);
    explicit MultiProgressBar(int min, int max, QObject *parent = nullptr);

    void addProgressBar(QProgressBar* progressBar);
    void setRange(int min, int max);
signals:
    void valueChanged(int value);

public slots:

private:
    int min;
    int max;
    QSet<QProgressBar*> set;
};

#endif // MULTIPROGRESSBAR_H
