﻿#ifndef MULTIBUTTONGROUP_H
#define MULTIBUTTONGROUP_H

#include "multibutton.h"

#include <QObject>
#include <QPushButton>

class MultiButtonGroup : public QObject {
    Q_OBJECT
public:
    explicit MultiButtonGroup(QObject* parent = nullptr);

    void addButton(const QString& action, QPushButton* btn);
    const QString& getChecked();
    void setChecked(const QString& action);
    void click(const QString& action);
signals:
    void buttonClicked(const QString& action);
    void buttonToggled(const QString& action);

private:
    QMap<QString, MultiButton*> actionBtnMap;
    QString checkedAction;
};

#endif // MULTIBUTTONGROUP_H
