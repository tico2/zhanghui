﻿#ifndef MULTISLIDER_H
#define MULTISLIDER_H

#include <QTimer>
#include <QSet>
#include <QSlider>

class MultiSlider : public QObject
{
    Q_OBJECT
public:
    explicit MultiSlider(QObject* parent = nullptr);
    explicit MultiSlider(QSlider* slider, QObject* parent = nullptr);

    void addSlider(QSlider* slider);
    void setValue(int value);
    void setRange(int min, int max);
signals:
    void sliderPressed();
    void sliderReleased();
    void valueChanged(int value);
    void sliderMoved(int value);

private:
    int min = 1;
    int max = 0;

    QSet<QSlider*> sliderSet;
};

#endif // MULTISLIDER_H
