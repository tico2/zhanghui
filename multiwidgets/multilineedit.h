﻿#ifndef MULTILINEEDIT_H
#define MULTILINEEDIT_H

#include <QObject>
#include <QLineEdit>
#include <QSet>

class MultiLineEdit : public QObject
{
    Q_OBJECT
public:
    explicit MultiLineEdit(QObject* parent = nullptr);
    explicit MultiLineEdit(QLineEdit* lineEdit, QObject* parent = nullptr);
    void addLineEdit(QLineEdit* lineEdit);
    void setText(const QString &text);

signals:
    void textChanged(const QString& text);

private:
    QSet<QLineEdit*> lineEditSet;
};

#endif // MULTILINEEDIT_H
