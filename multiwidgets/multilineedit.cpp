﻿#include "multilineedit.h"

MultiLineEdit::MultiLineEdit(QObject* parent) : QObject(parent) {}

MultiLineEdit::MultiLineEdit(QLineEdit* lineEdit, QObject* parent) : MultiLineEdit(parent) {
    addLineEdit(lineEdit);
}

void MultiLineEdit::addLineEdit(QLineEdit *lineEdit) {
    lineEditSet.insert(lineEdit);
    QObject::connect(lineEdit, &QLineEdit::textEdited, this, &MultiLineEdit::textChanged);
}

void MultiLineEdit::setText(const QString& text) {
    for (QLineEdit* lineEdit : lineEditSet) {
        lineEdit->setText(text);
    }
}
