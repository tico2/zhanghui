﻿#ifndef BUTTONS_H
#define BUTTONS_H

#include <QPushButton>
#include <QSet>
#include <QMap>

class MultiButton : public QObject
{
    Q_OBJECT
public:
    explicit MultiButton(QObject* parent = nullptr);
    explicit MultiButton(QPushButton* btn, QObject* parent = nullptr);

    void addButton(QPushButton* btn);
    void addButton(QPushButton* btn, bool on_off);
    bool isChecked();
    void setChecked(bool checked);
    void click();
signals:
    void pressed();
    void released();
    void clicked(bool checked);
    void toggled(bool checked);
private:
    QSet<QPushButton*> buttons;
    QSet<QPushButton*> reversedButtons;
    bool checked;
};

#endif // BUTTONS_H
