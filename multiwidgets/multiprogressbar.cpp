﻿#include "multiprogressbar.h"

MultiProgressBar::MultiProgressBar(int min, int max, QObject *parent) : QObject(parent) {
    setRange(min, max);
}

MultiProgressBar::MultiProgressBar(int min, int max, QProgressBar* progressBar, QObject *parent) : MultiProgressBar(min, max, parent) {
    addProgressBar(progressBar);
}

void MultiProgressBar::addProgressBar(QProgressBar* progressBar) {
    QObject::connect(progressBar, &QProgressBar::valueChanged, this, [=](int value) {
        emit valueChanged(value < min ? min : (value > max ? max : value));
    });
    set.insert(progressBar);
}

void MultiProgressBar::setRange(int min, int max) {
    if (min > max) {
        return;
    }
    this->min = min;
    this->max = max;
    for (QProgressBar* bar : set) {
        bar->setRange(min, max);
    }
}
