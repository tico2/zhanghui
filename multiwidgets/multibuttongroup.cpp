﻿#include "multibuttongroup.h"

MultiButtonGroup::MultiButtonGroup(QObject* parent) : QObject(parent) {}

void MultiButtonGroup::addButton(const QString& action, QPushButton* btn) {
    MultiButton* multiButton = actionBtnMap.value(action);
    if (multiButton == nullptr) {
        multiButton = new MultiButton(this);
        QObject::connect(multiButton, &MultiButton::clicked, this, [=] { click(action); });
        actionBtnMap.insert(action, multiButton);
    }
    multiButton->addButton(btn);
}

const QString& MultiButtonGroup::getChecked() {
    return checkedAction;
}

void MultiButtonGroup::setChecked(const QString& action) {
    if (this->checkedAction == action) {
        return;
    }
    checkedAction = action;
    emit buttonToggled(action);
    for (QMap<QString, MultiButton*>::iterator it = actionBtnMap.begin(); it != actionBtnMap.end(); it++) {
        it.value()->setChecked(it.key() == action);
    }
}

void MultiButtonGroup::click(const QString &action) {
    emit buttonClicked(action);
    setChecked(action);
}
