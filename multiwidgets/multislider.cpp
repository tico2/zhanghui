﻿#include "multislider.h"

MultiSlider::MultiSlider(QObject* parent) : QObject(parent) {}

MultiSlider::MultiSlider(QSlider* slider, QObject* parent) : MultiSlider(parent) {
    addSlider(slider);
}

void MultiSlider::addSlider(QSlider *slider) {
    if (min <= max) {
        slider->setRange(min, max);
    }
    sliderSet.insert(slider);
    QObject::connect(slider, &QSlider::sliderPressed, this, &MultiSlider::sliderPressed);
    QObject::connect(slider, &QSlider::sliderReleased, this, &MultiSlider::sliderReleased);
    QObject::connect(slider, &QSlider::valueChanged, this, &MultiSlider::valueChanged);
    QObject::connect(slider, &QSlider::sliderMoved, this, &MultiSlider::sliderMoved);\
}

void MultiSlider::setValue(int value) {
    value = value < min ? min : (value > max ? max : value);
    for (QSlider* slider : sliderSet) {
        slider->setValue(value);
    }
}

void MultiSlider::setRange(int min, int max) {
    this->min = min;
    this->max = max;
    if (min > max) {
        return;
    }
    for (QSlider* slider : sliderSet) {
        slider->setRange(min, max);
    }
}
