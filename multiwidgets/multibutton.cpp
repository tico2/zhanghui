﻿#include "multibutton.h"

MultiButton::MultiButton(QObject* parent) : QObject(parent) {}

MultiButton::MultiButton(QPushButton* btn, QObject* parent) : MultiButton(parent) {
    addButton(btn);
}

void MultiButton::addButton(QPushButton* btn) {
    buttons.insert(btn);
    QObject::connect(btn, &QPushButton::clicked, this, [=](bool checked) {
        emit clicked(checked);
        setChecked(checked);
    });
    QObject::connect(btn, &QPushButton::pressed, this, &MultiButton::pressed);
    QObject::connect(btn, &QPushButton::released, this, &MultiButton::released);
}

void MultiButton::addButton(QPushButton* btn, bool on_off) {
    if (on_off) {
        buttons.insert(btn);
    } else {
        reversedButtons.insert(btn);
    }
    QObject::connect(btn, &QPushButton::clicked, this, [=](bool checked) {
        if (!checked) {
            btn->setChecked(true);
            return;
        }
        emit clicked(!(on_off ^ checked));
        setChecked(!(on_off ^ checked));
    });
    QObject::connect(btn, &QPushButton::pressed, this, &MultiButton::pressed);
    QObject::connect(btn, &QPushButton::released, this, &MultiButton::released);
}

bool MultiButton::isChecked() {
    return checked;
}

void MultiButton::setChecked(bool checked) {
    if (this->checked == checked) {
        return;
    }
    emit toggled(checked);
    this->checked = checked;
    for (QPushButton* btn : buttons) {
        btn->setChecked(checked);
    }
    for (QPushButton* btn : reversedButtons) {
        btn->setChecked(!checked);
    }
}

void MultiButton::click() {
    emit clicked(!checked);
    setChecked(!checked);
}
