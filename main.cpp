﻿#include "mainwindow.h"

#include <QApplication>
#include "devices/devicemanager.h"
#include "devices/biamp.h"
#include "projectdevice.h"

void iniDevices(){
    //Biamp设备
    Biamp *biamp = new Biamp("","");    //Biamp处理器对象    Leveler(滑条) x 2 ; LevelMeter(电平表) x 4;
    //推子
    biamp->addModule(new BiampLeveler("默认输出通道1 Leveler","LevelOut01",1,-40,12));
    biamp->addModule(new BiampLeveler("默认输出通道2 Leveler","LevelOut02",2,-40,12));
    //电平表
    biamp->addModule(new BiampLevelMeter("默认输出通道1L Meter","AudioOut01",1,-40,12));
    biamp->addModule(new BiampLevelMeter("默认输出通道1R Meter","AudioOut01",2,-40,12));
    biamp->addModule(new BiampLevelMeter("默认输出通道2L Meter","AudioOut02",1,-40,12));
    biamp->addModule(new BiampLevelMeter("默认输出通道2R Meter","AudioOut02",2,-40,12));

    //Biamp矩阵
    ProjectBiampMatrix *matrix = new ProjectBiampMatrix("BiampMatrix","AVRouter");     //Biamp视频矩阵对象，继承自Biamp
    biamp->addModule(matrix);//添加矩阵设备
    //添加矩阵预览流 5 In 2 Out
    matrix->addInput(1, "");
    matrix->addInput(2, "");
    matrix->addInput(3, "");
    matrix->addInput(4, "");
    matrix->addInput(5, "");
    matrix->addOutput(1, "");
    matrix->addOutput(2, "");

    biamp->connectToHost();     //连接biamp处理器
}

int main(int argc, char *argv[])
{
    iniDevices();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    int result = a.exec();
    DeviceManager::removeAllDevices();
    return result;
}
