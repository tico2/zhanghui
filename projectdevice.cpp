﻿#include "projectdevice.h"

ProjectBiampMatrix::ProjectBiampMatrix(QString moduleName, QString moduleTag) : BiampAVRouter(moduleName, moduleTag) {}

void ProjectBiampMatrix::switchTo(int input, int output) {
    //TODO 修改input output port
    if (output <= 2) {
        executeCommand("AVRouter1 set input " + QString::number(output) + ' ' + QString::number(input));
    } else {
        if (input > 2) {
            executeCommand("AVRouter2 set input " + QString::number(output-2) + ' ' + QString::number(input-2));
        } else {
            qDebug() << "Invalid switch:" << input << "->" << output;
            return;
        }
    }
    qDebug() << name + ":" << input << "->" << output;
}

//ProjectCrestronMatrix::ProjectCrestronMatrix(QString deviceName, QList<VideoMatrixInfo> inputInfoSet, QList<VideoMatrixInfo> outputInfoSet) : CrestronMatrix(deviceName, inputInfoSet, outputInfoSet) {}

//void ProjectCrestronMatrix::switchTo(int input, int output) {
//    //TODO 修改input output port
//    qDebug() << name + ":" << input << "->" << output;
//    QString command = SWITCH_COMMAND;
//    command.replace("[input]", QString::number(input));
//    command.replace("[output]", QString::number(output));
//    executeCommand(command);
//}

//ProjectCrestronCamera::ProjectCrestronCamera(QString moduleName, int cameraId, CameraControlBtns &controlBtns) : CrestronCamera(moduleName, controlBtns), cameraId(cameraId) {}

//void ProjectCrestronCamera::zoom(QString action) {
//    move(action);
//}

//void ProjectCrestronCamera::move(QString direction) {
//    QString command = MOVE_COMMAND;
//    command.replace("[direction]", direction);
//    command.replace("[cameraId]", QString::number(cameraId));
//    executeCommand(command);
//}

//void ProjectCrestronCamera::prePos(QString action, int presetId) {
//    QString command = PRESET_COMMAND;
//    command.replace("[action]", action);
//    command.replace("[position]", QString::number(presetId));
//    executeCommand(command);
//}

//ProjectCrestronLeveler::ProjectCrestronLeveler(QString moduleName, int channelId, QSlider* levelSlider, QPushButton* muteBtn) : CrestronLeveler(moduleName, channelId, levelSlider, muteBtn) {}

//void ProjectCrestronLeveler::setLevel(int level) {
//    QString command = SET_LEVEL_COMMAND;
//    command.replace("[channel]", QString::number(channelId));
//    int val = (level + 40) / 52 * 65535;
//    command.replace("[level]", QString::number(val));
//    executeCommand(command);
//}

//void ProjectCrestronLeveler::setMute(bool on) {
//    QString command = SET_MUTE_COMMAND;
//    command.replace("[channel]", QString::number(channelId));
//    command.replace("[mute]", on ? "on" : "off");
//    executeCommand(command);
//}
