﻿#ifndef PROJECTDEVICE_H
#define PROJECTDEVICE_H

#include "devices/biamp.h"
#include "devices/crestron.h"

class ProjectBiampMatrix : public BiampAVRouter {
    Q_OBJECT
public:
    explicit ProjectBiampMatrix(QString deviceName, QString tag);
    void switchTo(int input, int output) override;
};

//class ProjectCrestronMatrix : public CrestronMatrix {
//    Q_OBJECT
//public:
//    explicit ProjectCrestronMatrix(QString deviceName, QList<VideoMatrixInfo> inputMap, QList<VideoMatrixInfo> outputMap);
//    void switchTo(int input, int output) override;
//protected:
//    const QString SWITCH_COMMAND = "Matrix [output] [input]";
//};

//class ProjectCrestronCamera : public CrestronCamera {
//    Q_OBJECT
//public:
//    explicit ProjectCrestronCamera(QString moduleName, int cameraId, CameraControlBtns &controlBtns);
//protected:
//    const int cameraId;
//    const QString MOVE_COMMAND = "CameraMove [cameraId] [direction]";
//    const QString PRESET_COMMAND = "CameraPreset [position] [action]";
//    void move(QString where) override;
//    void zoom(QString where) override;
//    void prePos(QString action, int presetId) override;
//};

//class ProjectCrestronLeveler : public CrestronLeveler {
//    Q_OBJECT
//public:
//    explicit ProjectCrestronLeveler(QString moduleName, int channelId, QSlider* levelSlider, QPushButton* muteBtn);
//protected:
//    const QString SET_LEVEL_COMMAND = "AudioLevel [channel] [level]";
//    const QString SET_MUTE_COMMAND = "AudioMute [channel] [mute]";
//    void setLevel(int level) override;
//    void setMute(bool on) override;
//};

#endif // PROJECTDEVICE_H
