﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVideoWidget>
#include <QMouseEvent>
#include <QDrag>
#include "projectdevice.h"
#include "devices/devicemanager.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MainWindow::showFullScreen();

    // Biamp
    Biamp* biamp = static_cast<Biamp*>(DeviceManager::getDevice("Biamp"));

    static_cast<BiampLeveler*>(biamp->getModule("默认输出通道1 Leveler"))->addControlWidgets(ui->output_1_level_slider, ui->output_1_mute_btn);
    static_cast<BiampLeveler*>(biamp->getModule("默认输出通道2 Leveler"))->addControlWidgets(ui->output_2_level_slider,ui->output_2_mute_btn);

    static_cast<BiampLevelMeter*>(biamp->getModule("默认输出通道1L Meter"))->addWidgets2(ui->output_1L_level_meter, ui->output_1L_level_meter_2);
    static_cast<BiampLevelMeter*>(biamp->getModule("默认输出通道1R Meter"))->addWidgets2(ui->output_1R_level_meter, ui->output_1R_level_meter_2);
    static_cast<BiampLevelMeter*>(biamp->getModule("默认输出通道2L Meter"))->addWidgets2(ui->output_2L_level_meter, ui->output_2L_level_meter_2);
    static_cast<BiampLevelMeter*>(biamp->getModule("默认输出通道2R Meter"))->addWidgets2(ui->output_2R_level_meter, ui->output_2R_level_meter_2);

    ProjectBiampMatrix *matrix = static_cast<ProjectBiampMatrix*>(biamp->getModule("BiampMatrix"));
    matrix->setInputVideoFrame(QStringLiteral("默认输入1"), 1 ,ui->matrix_input_1 );
    matrix->setInputVideoFrame(QStringLiteral("默认输入2"), 2 ,ui->matrix_input_2 );
    matrix->setInputVideoFrame(QStringLiteral("默认输入3"), 3 ,ui->matrix_input_3 );
    matrix->setInputVideoFrame(QStringLiteral("默认输入4"), 4 ,ui->matrix_input_4 );
    matrix->setInputVideoFrame(QStringLiteral("默认输入5"), 5 ,ui->matrix_input_5 );
    matrix->setOutputVideoFrame(QStringLiteral("默认输出1"), 1,ui->matrix_output_1);
    matrix->setOutputVideoFrame(QStringLiteral("默认输出2"), 2, ui->matrix_output_2);
    matrix->playAll();

}

MainWindow::~MainWindow()
{
    delete ui;
}

