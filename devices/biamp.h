﻿#ifndef BIAMP_H
#define BIAMP_H

#include <QTimer>
#include <QProgressBar>
#include <QPushButton>
#include <QSlider>
#include <QLineEdit>
#include <QGroupBox>
#include "device.h"
#include "audio.h"
#include "matrix.h"

class Biamp;
class BiampModule;
class SubscribableBiampModule;

class Biamp : public Device {
    Q_OBJECT
public:
    explicit Biamp(const QString& name, const QString& hostname, unsigned short port = 23, const QString& username = "", const QString& password = "");
    ~Biamp() override;
    void connectToHost();
    void addModule(BiampModule* module);
    void addModule(SubscribableBiampModule* module);
    BiampModule* getModule(const QString& moduleName);
    void executeCommand(const QString& command);
private:
    TelnetConnection* connection;
    void handleMessage(const QString& msg);
    void initialize();
    QSet<BiampModule*> moduleSet;
    QSet<SubscribableBiampModule*> subscribableModuleSet;
    QMap<QString, SubscribableBiampModule*> subscribableModuleMap;
    QString username;
    QString password;
};

class BiampModule {
public:
    explicit BiampModule(const QString& moduleName, const QString& moduleTag);
    virtual ~BiampModule();
    void setParent(Biamp* parent);

    const QString name;
    const QString tag;
protected:
    Biamp* biamp;

    void executeCommand(const QString& command);
};

class SubscribableBiampModule : public BiampModule {
public:
    explicit SubscribableBiampModule(const QString& moduleName, const QString& moduleTag);
    const QString subscribe(const QString& action, int channel);
    virtual QStringList subscribeAll() = 0;
    virtual void handleMessage(const QString& msg) = 0;
private:
    const int subscribeTime = 100;
};

class BiampLeveler : public Leveler, public SubscribableBiampModule {
public:
    explicit BiampLeveler(const QString& moduleName, const QString& moduleTag, int channelId, int min, int max);
    QStringList subscribeAll() override;
    void handleMessage(const QString& msg) override;
    void setLevel(int level) override;
    void setMute(bool mute) override;
};

class BiampLevelMeter : public LevelMeter, public SubscribableBiampModule {
public:
    explicit BiampLevelMeter(const QString& moduleName, const QString& moduleTag, int channelId, int min, int max);
    QStringList subscribeAll() override;
    void handleMessage(const QString& msg) override;
};

class BiampAVRouter : public VideoMatrix, public BiampModule {
    Q_OBJECT
public:
    explicit BiampAVRouter(const QString& moduleName, const QString& moduleTag);
    void switchTo(int input, int output) override;
};

class BiampMuteControl : public MuteControl, public SubscribableBiampModule {
    Q_OBJECT
public:
    explicit BiampMuteControl(const QString& moduleName, const QString& moduleTag, int channelId);

    void setMute(bool mute) override;
    QStringList subscribeAll() override;
    void handleMessage(const QString& msg) override;
};

#endif // BIAMP_H
