﻿#ifndef MATRIX_H
#define MATRIX_H

#include <QUrl>
#include <QGridLayout>
#include <QFrame>

#include "widgets/videoframe.h"
#include "device.h"

struct VideoInfo
{
    VideoInfo() {}

    QString videoName;
    int channel;

};

class Matrix : public Device {
    Q_OBJECT
public:
    explicit Matrix(const QString& deviceName);
    virtual void switchTo(int input, int output) = 0;
};

class VideoMatrix : public Matrix {
    Q_OBJECT
public:
    explicit VideoMatrix(const QString& deviceName);
    virtual ~VideoMatrix() override;

    void addInput(int channel, const QString& url);
    void addOutput(int channel, const QString& url);
    void addOutput(int channel);
    void setInputVideoFrame(const QString& videoName, int channel, InputVideoFrame* videoFrame);
    void setOutputVideoFrame(const QString& videoName, int channel, OutputVideoFrame* videoFrame);

    void playAll();
//    void stopAll();
private:
    QMap<int, QString> inputUrlMap;
    QMap<int, QString> outputUrlMap;
    QSet<VideoFrame*> playerSet;
};

#endif // MATRIX_H
