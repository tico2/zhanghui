﻿#ifndef CAMERA_H
#define CAMERA_H

#include "device.h"

#include <QPushButton>

class Camera : public Device {
    Q_OBJECT
public:
    explicit Camera(const QString& deviceName);
    void setDirectionButtons(QPushButton* upBtn, QPushButton* downBtn, QPushButton* leftBtn, QPushButton* rightBtn, QPushButton* homeBtn);
    void setZoomButtons(QPushButton* zoomTeleBtn, QPushButton* zoomWideBtn);
    void setPresetButton(QPushButton* presetBtn);
    void addPreset(int presetId, QPushButton* presetBtn);

    virtual void moveUp() = 0;
    virtual void moveDown() = 0;
    virtual void moveLeft() = 0;
    virtual void moveRight() = 0;
    virtual void moveHome() = 0;
    virtual void moveStop() = 0;

    virtual void zoomWide() = 0;
    virtual void zoomTele() = 0;
    virtual void zoomStop() = 0;

    virtual void setPreset(int pos) = 0;
    virtual void recallPreset(int pos) = 0;
protected:
    int currentPresetId;
    QMap<int, QPushButton*> presetBtnMap;
    void clearPresetSelect();
};

#endif // CAMERA_H
