﻿#ifndef MAXHUB_H
#define MAXHUB_H

#include "device.h"

class MaxHub : public Device
{
public:
    explicit MaxHub(const QString& deviceName, const QString& host, const QString& serialNumber, const QString& magicCode);
    ~MaxHub() override;
//    QMap<QString, QString> getInfo();
    bool shutdown();
    bool getScreenStatus();
    bool setScreenStatus(bool on);
    float getVolume();
    bool setVolume(float volume);
    float getBrightness();
    bool setBrightness(float brightness);
    int getChannel();
    bool setChannel(int channel);

protected:
    QUrlQuery getRequestParam();
private:
    BlockingHttpConnection* connection;
    const QString hostname;
    const QString serialNumber;
    const QString magicCode;
    bool query(QString path, QString* reponse = nullptr);
    bool execute(QString path, QString key, QString value);
    bool execute(QString path);

    QJsonValue getJsonValue(const QString& json, QString key);
    inline BlockingHttpConnection* getConnection();
};

#endif // MAXHUB_H
