﻿#include "matrix.h"
#include <QLabel>

Matrix::Matrix(const QString& deviceName) : Device(deviceName) {}

VideoMatrix::VideoMatrix(const QString& deviceName) : Matrix(deviceName) {}

VideoMatrix::~VideoMatrix() {}

void VideoMatrix::addInput(int channel, const QString& url) {
    inputUrlMap.insert(channel, url);
}

void VideoMatrix::addOutput(int channel, const QString& url) {
    outputUrlMap.insert(channel, url);
}

void VideoMatrix::addOutput(int channel) {
    outputUrlMap.insert(channel, "");
}

void VideoMatrix::setInputVideoFrame(const QString& videoName, int channel, InputVideoFrame* videoFrame) {
    videoFrame->setInfo(this, videoName, channel, inputUrlMap.value(channel));
    playerSet.insert(videoFrame);
}

void VideoMatrix::setOutputVideoFrame(const QString& videoName, int channel, OutputVideoFrame* videoFrame) {
    videoFrame->VideoFrame::setInfo(this, videoName, channel, outputUrlMap.value(channel));
    playerSet.insert(videoFrame);
}


void VideoMatrix::playAll() {
    for (VideoFrame* player : playerSet.values()) {
        player->play();
    }
}

//void VideoMatrix::stopAll() {
//    for (VideoWidget* player : playerSet.values()) {
//        player->stop();
//    }
//}
