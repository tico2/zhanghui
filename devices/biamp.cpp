﻿#include "biamp.h"
#include <QRegExp>
#include <QDateTime>

Biamp::Biamp(const QString& deviceName, const QString& hostname, unsigned short port, const QString& username, const QString& password) : Device(deviceName) {
    this->username = username;
    this->password = password;

    connection = new TelnetConnection(hostname, port);
    connection->setConnectTimeout(10000);
    QObject::connect(connection, &TelnetConnection::connected, this, &Biamp::initialize);
    QObject::connect(connection, &TelnetConnection::receiveMessage, this, &Biamp::handleMessage);
}

Biamp::~Biamp() {
    for (BiampModule* module : moduleSet) {
        delete module;
    }
    delete connection;
}

void Biamp::connectToHost() {
    connection->connectToHost();
}

void Biamp::initialize() {
    for (SubscribableBiampModule* module : subscribableModuleSet) {
        QStringList tokenList = module->subscribeAll();
        for (QString token : tokenList) {
            subscribableModuleMap.insert(token, module);
        }
    }
}

void Biamp::handleMessage(const QString& msg) {
    if (msg.contains("Welcome to the Tesira Text Protocol Server...")) {
        emit connected();
        return;
    }
    QRegExp re("! \"publishToken\":\"([A-Za-z0-9_\\-.]+)\" \"value\":([a-z0-9\\-.]+)");
    if (re.indexIn(msg) >= 0) {
        QString token = re.cap(1);
        SubscribableBiampModule* subscribableModule = subscribableModuleMap.value(token);
        if (subscribableModule != nullptr) {
//            qDebug("[%s] Receive: %s", qPrintable(name), qPrintable(msg));
            subscribableModule->handleMessage(re.cap(2));
        } else {
            qDebug("[%s] Cannot find token(won't happen): %s", qPrintable(id), qPrintable(msg));
        }
    } else {
        qDebug("[%s] Receive(unhandled): %s", qPrintable(id), qPrintable(msg));
    }
}

void Biamp::executeCommand(const QString& command) {
    connection->write(command.toUtf8());
    qDebug("[%s] send: %s", qPrintable(id), qPrintable(command));
}

void Biamp::addModule(BiampModule* module) {
    module->setParent(this);
    moduleSet.insert(module);
    qDebug("[%s] add module: %s", qPrintable(id), qPrintable(module->tag));
}

void Biamp::addModule(SubscribableBiampModule* module) {
    addModule(static_cast<BiampModule*>(module));
    if (connection->isConnected()) {
        QStringList tokenList = module->subscribeAll();
        for (QString token : tokenList) {
            subscribableModuleMap.insert(token, module);
        }
    } else {
        subscribableModuleSet.insert(module);
    }
}

BiampModule* Biamp::getModule(const QString& moduleName) {
    for (BiampModule* module : moduleSet) {
        if (module->name == moduleName) {
            return module;
        }
    }
    for (SubscribableBiampModule* module : subscribableModuleSet) {
        if (module->name == moduleName) {
            return module;
        }
    }
    return nullptr;
}

BiampModule::BiampModule(const QString& moduleName, const QString& moduleTag) : name(moduleName), tag(moduleTag) {}

BiampModule::~BiampModule() {}

void BiampModule::setParent(Biamp *parent) {
    this->biamp = parent;
}

void BiampModule::executeCommand(const QString& command) {
    biamp->executeCommand(command);
}

SubscribableBiampModule::SubscribableBiampModule(const QString& moduleName, const QString& moduleTag) : BiampModule (moduleName, moduleTag) {}

const QString SubscribableBiampModule::subscribe(const QString& action, int channel) {
    const QString token = tag + '_' + QString::number(channel) + '_' + action;
    QString command = tag + " subscribe " + action + ' ' + QString::number(channel) + ' ' + token + ' ' + QString::number(subscribeTime);
    executeCommand(command);
    return token;
}

BiampLeveler::BiampLeveler(const QString& moduleName, const QString& moduleTag, int channel, int min, int max) : Leveler(moduleName, channel, min, max), SubscribableBiampModule (moduleName, moduleTag) {}

QStringList BiampLeveler::subscribeAll() {
    QStringList tokenList;
    tokenList.append(subscribe("mute", channelId));
    tokenList.append(subscribe("level", channelId));
    return tokenList;
}

void BiampLeveler::handleMessage(const QString& msg) {
    if (msg == "true") {
        emit muteChanged(true);
    } else if (msg == "false") {
        emit muteChanged(false);
    } else {
        updateLevel(static_cast<int>(msg.toFloat()));
    }
}

void BiampLeveler::setMute(bool mute) {
    executeCommand(tag + " set mute " + QString::number(channelId) + (mute ? " true" : " false"));
}

void BiampLeveler::setLevel(int level) {
    executeCommand(tag + " set level " + QString::number(channelId) + ' ' + QString::number(level));
}

BiampLevelMeter::BiampLevelMeter(const QString& moduleName, const QString& moduleTag, int channel, int min, int max) : LevelMeter (moduleName, channel, min, max), SubscribableBiampModule (moduleName, moduleTag) {}

QStringList BiampLevelMeter::subscribeAll() {
    return QStringList(subscribe("level", channelId));
}

void BiampLevelMeter::handleMessage(const QString& msg) {
    emit meterValueChanged(static_cast<int>(msg.toFloat()));
}

BiampAVRouter::BiampAVRouter(const QString& moduleName, const QString& moduleTag) : VideoMatrix (moduleName), BiampModule(moduleName, moduleTag) {}

void BiampAVRouter::switchTo(int input, int output) {
    executeCommand(tag + " set input " + QString::number(output) + ' ' + QString::number(input));
}

BiampMuteControl::BiampMuteControl(const QString& moduleName, const QString& moduleTag, int channel) : MuteControl (moduleName, channel), SubscribableBiampModule(moduleName, moduleTag) {}

void BiampMuteControl::setMute(bool mute) {
    executeCommand(tag + " set mute " + QString::number(channelId) + (mute ? " true" : " false"));
}

QStringList BiampMuteControl::subscribeAll() {
    return QStringList(subscribe("mute", channelId));
}

void BiampMuteControl::handleMessage(const QString &msg) {
    emit muteChanged(msg == "true");
}

