﻿#include "maxhub.h"

MaxHub::MaxHub(const QString& deviceName, const QString& host, const QString& sn, const QString& mc) : Device(deviceName), hostname(host), serialNumber(sn), magicCode(mc) {
    connection = new BlockingHttpConnection();
//    QObject::connect(connection, &TcpConnection::connected, this, &Crestron::getInfo);
}

MaxHub::~MaxHub() {
    delete connection;
}

QUrlQuery MaxHub::getRequestParam() {
    QString timestamp = QString::number(QDateTime::currentDateTime().toTime_t());
    QString s = serialNumber + timestamp + magicCode;
    QUrlQuery query;
    query.addQueryItem("token", QString(QCryptographicHash::hash(s.toUtf8(), QCryptographicHash::Md5).toHex()));
    query.addQueryItem("ts", timestamp);
    return query;
}

bool MaxHub::query(QString path, QString* response) {
    return getConnection()->get("http://" + hostname + path + '?' + getRequestParam().query(), response) == 200;
}

bool MaxHub::execute(QString path, QString key, QString value) {
    QUrlQuery params = getRequestParam();
    params.addQueryItem(key, value);
    return getConnection()->post("http://" + hostname + path, params.query().toUtf8()) == 200;
}

bool MaxHub::execute(QString path) {
    return getConnection()->post("http://" + hostname + path, getRequestParam().query().toUtf8()) == 200;
}

//QMap<QString, QString> MaxHub::getInfo() {
//    QString resp = executeCommand("/infomations");
//    if () {
//        //TODO
//    }
//}

bool MaxHub::shutdown() {
    return execute("/shutdown");
}

bool MaxHub::getScreenStatus() {
    QString response;
    if (query("/screen", &response)) {
        QJsonValue jsonValue = getJsonValue(response.toUtf8(), "state");
        if (jsonValue != QJsonValue::Undefined) {
            return jsonValue.toBool(false);
        }
    }
    return false;
}

bool MaxHub::setScreenStatus(bool on) {
    return execute("/screen", "state", on ? "true" : "false");
}

float MaxHub::getVolume() {
    QString response;
    if (query("/volume", &response)) {
        QJsonValue jsonValue = getJsonValue(response, "volume");
        if (jsonValue != QJsonValue::Undefined) {
            return static_cast<float>(jsonValue.toDouble(-101));
        }
    }
    return -1;
}

bool MaxHub::setVolume(float volume) {
    return execute("/volume", "volume", QString::number(static_cast<double>(volume)));
}

float MaxHub::getBrightness() {
    QString response;
    if (query("/brightness", &response)) {
        QJsonValue jsonValue = getJsonValue(response, "brightness");
        if (jsonValue != QJsonValue::Undefined) {
            return static_cast<float>(jsonValue.toDouble(-101));
        }
    }
    return -1;
}

bool MaxHub::setBrightness(float brightness) {
    return execute("/brightness", "brightness", QString::number(static_cast<double>(brightness)));
}

int MaxHub::getChannel() {
    QString response;
    if (query("/input", &response)) {
        QJsonValue jsonValue = getJsonValue(response, "current_input_id");
        if (jsonValue != QJsonValue::Undefined) {
            return jsonValue.toInt(-1);
        }
    }
    return -1;
}

bool MaxHub::setChannel(int channel) {
    return execute("/input", "input_id", QString::number(channel));
}

QJsonValue MaxHub::getJsonValue(const QString& json, QString key) {
    QJsonParseError parseError;
    QJsonDocument document = QJsonDocument::fromJson(json.toUtf8(), &parseError);
    if (!document.isNull() && (parseError.error == QJsonParseError::NoError)) {
        QJsonObject obj = document.object();
        return obj.value(key);
    }
    return QJsonValue::Undefined;
}

inline BlockingHttpConnection* MaxHub::getConnection() {
    return static_cast<BlockingHttpConnection*>(connection);
}
