﻿#include "toggle.h"

Toggle::Toggle(const QString& deviceName, QPushButton* btn) : Device(deviceName) {
    this->btn = btn;
    btn->setCheckable(true);
    QObject::connect(btn, &QPushButton::clicked, this, &Toggle::toggle);
}

void Toggle::setChecked(bool checked) {
    return btn->setChecked(checked);
}

bool Toggle::isChecked() {
    return btn->isChecked();
}
