﻿#include "audio.h"

AudioChannel::AudioChannel(int channel) {
    this->channelId = channel;
}

int AudioChannel::getChannelId() {
    return channelId;
}

void AudioChannel::setChannelId(int channelId) {
    this->channelId = channelId;
}

Leveler::Leveler(const QString& moduleName, int channel, int minLevel, int maxLevel) : Device(moduleName), AudioChannel (channel) {
    multiSlider->setRange(minLevel, maxLevel);

    QObject::connect(this, &Leveler::levelChanged, volEdit, [=](int value) {
        currentLevel = value;
        volEdit->setText(QString::number(value));
        if (levelSliderValueChangeable) {
            multiSlider->setValue(value);
        }
    });

    // Volume Slider
    QObject::connect(&levelSliderTimer, &QTimer::timeout, this, [=] { setLevel(currentLevel + step); });
    QObject::connect(multiSlider, &MultiSlider::sliderPressed, this, [=] {
        setLevel(currentLevel + step);
        levelUpTimer.start(300);
        levelSliderValueChangeable = false;
    });
    QObject::connect(multiSlider, &MultiSlider::sliderReleased, this, [=] { levelUpTimer.stop(); levelSliderValueChangeable = true; });

    // Volume Button
    QObject::connect(&levelUpTimer, &QTimer::timeout, this, [=] { setLevel(currentLevel + step); });
    QObject::connect(volUpBtn, &MultiButton::pressed, this, [=] { setLevel(currentLevel + step); levelUpTimer.start(300); });
    QObject::connect(volUpBtn, &MultiButton::released, this, [=] { levelUpTimer.stop(); });

    QObject::connect(&levelDownTimer, &QTimer::timeout, this, [=] { setLevel(currentLevel - step); });
    QObject::connect(volDownBtn, &MultiButton::pressed, this, [=] { setLevel(currentLevel - step); levelDownTimer.start(300); });
    QObject::connect(volDownBtn, &MultiButton::released, this, [=] { levelDownTimer.stop(); });

    // Mute Button
    QObject::connect(this, &Leveler::muteChanged, muteBtn, &MultiButton::setChecked);
    QObject::connect(muteBtn, &MultiButton::clicked, this, &Leveler::setMute);
}

void Leveler::updateLevel(int level) {
    currentLevel = level;
    emit levelChanged(level);
}

void Leveler::addLevelSlider(QSlider* slider) {
    multiSlider->addSlider(slider);
}

void Leveler::addMuteButton(QPushButton* muteBtn) {
    this->muteBtn->addButton(muteBtn);
}

void Leveler::addControlWidgets(QSlider* levelSlider, QPushButton* muteBtn) {
    addLevelSlider(levelSlider);
    addMuteButton(muteBtn);
}

void Leveler::addVolumeButton(QPushButton* volumeUp, QPushButton* volumeDown, QLineEdit* levelEdit, int step) {
    this->volUpBtn->addButton(volumeUp);
    this->volDownBtn->addButton(volumeDown);
    this->volEdit->addLineEdit(levelEdit);
    this->step = step;
}

LevelMeter::LevelMeter(const QString& moduleName, int channel, int min, int max) : Device(moduleName), AudioChannel (channel) {
    this->min = min;
    this->max = max;
}

void LevelMeter::addMeterBar(QProgressBar *meterBar) {
    QObject::connect(this, &LevelMeter::meterValueChanged, this, [=] (int value) {
        meterBar->setValue(value < min ? min : (value > max ? max : value));
        meterBar->repaint();
    });
}

void LevelMeter::addLabel(QLabel *label) {
    QObject::connect(this, &LevelMeter::meterValueChanged, this, [=] (int value) {
        label->setText(QString::number(value < min ? min : (value > max ? max : value)));
        label->repaint();
    });
}

void LevelMeter::addLineEdit(QLineEdit *lineEdit) {
    QObject::connect(this, &LevelMeter::meterValueChanged, this, [=] (int value) {
        lineEdit->setText(QString::number(value < min ? min : (value > max ? max : value)));
        lineEdit->repaint();
    });
}

void LevelMeter::addWidgets1(QProgressBar* meterBar, QLabel* label) {
    addMeterBar(meterBar);
    addLabel(label);
}

void LevelMeter::addWidgets2(QProgressBar* meterBar, QLineEdit* lineEdit) {
    addMeterBar(meterBar);
    addLineEdit(lineEdit);
}

MuteControl::MuteControl(const QString& moduleName, int channel) : Device(moduleName), AudioChannel (channel) {
    QObject::connect(this, &MuteControl::muteChanged, muteMultiBtn, &MultiButton::setChecked);
    QObject::connect(muteMultiBtn, &MultiButton::clicked, this, &MuteControl::setMute);
}

void MuteControl::addMuteButton(QPushButton* muteBtn) {
    muteMultiBtn->addButton(muteBtn);
}

