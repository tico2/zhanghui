﻿#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include "connections/blockinghttpconnection.h"
#include "connections/networkconnection.h"
#include "connections/noneblockinghttpconnection.h"
#include "connections/noneblockingnetworkconnection.h"
#include "connections/tcpconnection.h"
#include "connections/telnetconnection.h"

class Device : public QObject {
    Q_OBJECT
public:
    explicit Device(const QString& deviceId);

    const QString id;
signals:
    void connected();
    void disconnected();
};

#endif // DEVICE_H
