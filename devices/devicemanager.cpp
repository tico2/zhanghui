﻿#include "devicemanager.h"

QSet<Device*> DeviceManager::devices;

void DeviceManager::addDevice(Device* device) {
    devices.insert(device);
    qDebug("Device added: %s", device->id.toStdString().c_str());
}

Device* DeviceManager::getDevice(const QString& deviceId) {
    for (Device* device : devices) {
        if (deviceId == device->id) {
            return device;
        }
    }
    return nullptr;
}

void DeviceManager::removeAllDevices() {
    for (Device* device : devices) {
        delete device;
    }
    devices.clear();
}
