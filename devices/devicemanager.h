﻿#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include "device.h"
#include <QSet>

class DeviceManager
{
public:
    static void addDevice(Device* device);
    static Device* getDevice(const QString& nameOrUuid);
    static void removeAllDevices();
private:
    static QSet<Device*> devices;
};

#endif // DEVICEMANAGER_H
