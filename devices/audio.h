﻿#ifndef AUDIO_H
#define AUDIO_H

#include <QObject>
#include <QGridLayout>
#include <QMap>
#include <QSlider>
#include <QPushButton>
#include <QTimer>
#include <QProgressBar>
#include <QLineEdit>
#include <QLabel>

#include "device.h"
#include "multiwidgets/multibutton.h"
#include "multiwidgets/multislider.h"
#include "multiwidgets/multilineedit.h"
#include "multiwidgets/multiprogressbar.h"

class AudioChannel {
public:
    AudioChannel(int channelId);
    int getChannelId();
    void setChannelId(int channelId);
protected:
    int channelId;
};

class Leveler : public Device, public AudioChannel {
    Q_OBJECT
public:
    explicit Leveler(const QString& moduleName, int channelId, int minLevel, int maxLevel);
    virtual void setMute(bool mute) = 0;
    virtual void setLevel(int level) = 0;
    void setRange(int min, int max);
    void updateLevel(int level);

    void addLevelSlider(QSlider* levelSlider);
    void addMuteButton(QPushButton* muteBtn);
    void addControlWidgets(QSlider* levelSlider, QPushButton* muteBtn);
    void addVolumeButton(QPushButton* volumeUp, QPushButton* volumeDown, QLineEdit* levelEdit, int step = 1);

signals:
    void levelChanged(int level);
    void muteChanged(bool mute);

private:
    MultiSlider* multiSlider = new MultiSlider(this);
    MultiButton* volUpBtn = new MultiButton(this);
    MultiButton* volDownBtn = new MultiButton(this);
    MultiButton* muteBtn = new MultiButton(this);
    MultiLineEdit* volEdit = new MultiLineEdit(this);

    QTimer levelSliderTimer;
    QTimer levelUpTimer;
    QTimer levelDownTimer;
    bool levelSliderValueChangeable = true;
    int currentLevel = 0;
    int step = 1;
};

class LevelMeter : public Device, public AudioChannel {
    Q_OBJECT
public:
    explicit LevelMeter(const QString& moduleName, int channelId, int min, int max);

    void addMeterBar(QProgressBar* meterBar);
    void addLabel(QLabel* label);
    void addLineEdit(QLineEdit* lineEdit);
    void addWidgets1(QProgressBar* meterBar, QLabel* label);
    void addWidgets2(QProgressBar* meterBar, QLineEdit* lineEdit);
signals:
    void meterValueChanged(int value);
private:
    int min;
    int max;
};

class MuteControl : public Device, public AudioChannel {
    Q_OBJECT
public:
    explicit MuteControl(const QString& moduleName, int channelId);

    void addMuteButton(QPushButton* muteBtn);
    virtual void setMute(bool mute) = 0;

private:
    MultiButton* muteMultiBtn = new MultiButton(this);
signals:
    void muteChanged(bool mute);
};
#endif // AUDIO_H
