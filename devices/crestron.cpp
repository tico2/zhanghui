﻿#include "crestron.h"
#include <QtWidgets/QLabel>

Crestron::Crestron(const QString& deviceName, const QString& host, unsigned short port) : Device (deviceName) {
    connection = new TcpConnection(host, port);
    QObject::connect(connection, &TcpConnection::receiveMessage, this, &Crestron::handleMessage);
}

Crestron::~Crestron() {
    delete connection;
    for (QSet<CrestronModule*>* set : prefixModuleSetMap.values()) {
        for (CrestronModule* module : *set) {
            delete module;
        }
        delete set;
    }
}

void Crestron::connectToHost() { connection->connectToHost(); }

void Crestron::send(const QString& msg) {
    qDebug("[%s] send: %s", qPrintable(id), qPrintable(msg));
    connection->write(msg.toUtf8());
}

void Crestron::addModule(CrestronModule* module) {
    module->setParent(this);
    nameModuleMap.insert(module->name, module);
    QSet<CrestronModule*>* set = prefixModuleSetMap.value(module->prefix);
    if (set == nullptr) {
        set = new QSet<CrestronModule*>;
        prefixModuleSetMap.insert(module->prefix, set);
    }
    set->insert(module);
    qDebug("[%s] add module: %s", qPrintable(id), qPrintable(module->name));
}

CrestronModule* Crestron::getModule(const QString& moduleName) {
    return nameModuleMap.value(moduleName);
}

void Crestron::handleMessage(const QString& msg) {
    QStringList list = msg.split("_");
    QSet<CrestronModule*>* set = prefixModuleSetMap.value(list[0]);
    if (set == nullptr) {
        qDebug("[%s] receive(ERR): %s", qPrintable(id), qPrintable(msg));
    } else {
        for (CrestronModule* module : *set) {
            qDebug("[%s] %s: %s", qPrintable(id), qPrintable(module->name), qPrintable(msg));
            module->handleMessage(list[1]);
        }
    }
}

CrestronModule::CrestronModule(const QString& moduleName, const QString& moduleId) : name(moduleName), prefix(moduleId) {}
CrestronModule::~CrestronModule() {}
void CrestronModule::setParent(Crestron* parent) { this->crestron = parent; }
void CrestronModule::executeCommand(const QString& command) { crestron->send(prefix + '_' + command); }
