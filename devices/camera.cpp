﻿#include "camera.h"
#include <QLabel>
#include <QtGlobal>

Camera::Camera(const QString& deviceName) : Device(deviceName) {}

void Camera::setDirectionButtons(QPushButton *upBtn, QPushButton *downBtn, QPushButton *leftBtn, QPushButton *rightBtn, QPushButton *homeBtn) {
    QObject::connect(homeBtn, &QPushButton::clicked, this, &Camera::moveHome);

    QObject::connect(upBtn, &QPushButton::pressed, this, &Camera::moveUp);
    QObject::connect(leftBtn, &QPushButton::pressed, this, &Camera::moveLeft);
    QObject::connect(rightBtn, &QPushButton::pressed, this, &Camera::moveRight);
    QObject::connect(downBtn, &QPushButton::pressed, this, &Camera::moveDown);
    QObject::connect(upBtn, &QPushButton::released, this, &Camera::moveStop);
    QObject::connect(leftBtn, &QPushButton::released, this, &Camera::moveStop);
    QObject::connect(rightBtn, &QPushButton::released, this, &Camera::moveStop);
    QObject::connect(downBtn, &QPushButton::released, this, &Camera::moveStop);
}

void Camera::setZoomButtons(QPushButton *zoomTeleBtn, QPushButton *zoomWideBtn) {
    QObject::connect(zoomTeleBtn, &QPushButton::pressed, this, &Camera::zoomTele);
    QObject::connect(zoomWideBtn, &QPushButton::pressed, this, &Camera::zoomWide);

    QObject::connect(zoomTeleBtn, &QPushButton::released, this, &Camera::zoomStop);
    QObject::connect(zoomWideBtn, &QPushButton::released, this, &Camera::zoomStop);
}

void Camera::setPresetButton(QPushButton *presetBtn) {
    QObject::connect(presetBtn, &QPushButton::clicked, this, [=] {
        this->setPreset(currentPresetId);
    });
}

void Camera::addPreset(int presetId, QPushButton *presetBtn) {
    QObject::connect(presetBtn, &QPushButton::clicked, this, [=] {
        recallPreset(presetId);
        currentPresetId = presetId;
    });
}

void Camera::clearPresetSelect() {
    for (QPushButton* btn : presetBtnMap.values()) {
        btn->setChecked(false);
    }
}
