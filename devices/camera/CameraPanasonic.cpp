﻿#include "CameraPanasonic.h"
#include <QLabel>
#include <QtGlobal>

const QString CameraPanasonic::ZOOM_WIDE = "Z01";
const QString CameraPanasonic::ZOOM_STOP = "Z50";
const QString CameraPanasonic::ZOOM_TELE = "Z99";

const QString CameraPanasonic::MOVE_HOME = "APC7FFF7FFF";
const QString CameraPanasonic::MOVE_LEFT = "PTS3550";
const QString CameraPanasonic::MOVE_RIGHT = "PTS6550";
const QString CameraPanasonic::MOVE_UP = "PTS5065";
const QString CameraPanasonic::MOVE_DOWN = "PTS5035";
const QString CameraPanasonic::MOVE_STOP = "PTS5050";

const QString CameraPanasonic::PREPOS_SET = "M";
const QString CameraPanasonic::PREPOS_RECALL = "R";
const QString CameraPanasonic::PREPOS_QUERY = "S";

CameraPanasonic::CameraPanasonic(const QString& deviceName, const QString& host) : Camera(deviceName) {
    baseUrlRequest = "http://" + host + "/cgi-bin/aw_ptz?res=1&cmd=%23";

    connection = new NoneBlockingHttpConnection();
    QObject::connect(connection, &NoneBlockingHttpConnection::connected, this, &CameraPanasonic::initialize);
    QObject::connect(connection, &NoneBlockingHttpConnection::receiveMessage, this, &CameraPanasonic::handleMessage);
}

CameraPanasonic::~CameraPanasonic() {
    delete connection;
}

void CameraPanasonic::initialize() {
    executeAction(PREPOS_QUERY);
}

void CameraPanasonic::handleMessage(const QString& msg) {
    qDebug("[%s] receive: %s", qPrintable(id), qPrintable(msg));
    QRegExp re("([A-Za-z]+)([0-9]+)");
    if (re.indexIn(msg) >= 0) {
        QString s1 = re.cap(1);
        QString s2 = re.cap(2);
        int presetId = s2.toInt();
        if (s1 == 's') {
            QPushButton* presetBtn = presetBtnMap.value(presetId);
            if (presetBtn != nullptr) {
                clearPresetSelect();
                presetBtn->setChecked(true);
            } else {
                qWarning() << "预置位按钮不存在:" << presetId;
            }
        }
    }
}

void CameraPanasonic::executeAction(const QString& action) {
    static_cast<NoneBlockingHttpConnection*>(connection)->get(QUrl(baseUrlRequest + action));
    qDebug("[%s] execute: %s", qPrintable(id), qPrintable(action));
}

void CameraPanasonic::prePos(QString action, int pos) {
    QString posString = QString("%1").arg(pos, 2, 10, QLatin1Char('0'));
    executeAction(action + posString);
}

void CameraPanasonic::moveUp() {
    executeAction(MOVE_UP);
}

void CameraPanasonic::moveDown() {
    executeAction(MOVE_DOWN);
}

void CameraPanasonic::moveLeft() {
    executeAction(MOVE_LEFT);
}

void CameraPanasonic::moveRight() {
    executeAction(MOVE_RIGHT);
}

void CameraPanasonic::moveHome() {
    executeAction(MOVE_HOME);
}

void CameraPanasonic::moveStop() {
    executeAction(MOVE_STOP);
}

void CameraPanasonic::zoomWide() {
    executeAction(ZOOM_WIDE);
}

void CameraPanasonic::zoomTele() {
    executeAction(ZOOM_TELE);
}

void CameraPanasonic::zoomStop() {
    executeAction(ZOOM_STOP);
}

void CameraPanasonic::setPreset(int pos) {
    prePos(PREPOS_SET, pos);
}

void CameraPanasonic::recallPreset(int pos) {
    prePos(PREPOS_RECALL, pos);
}
