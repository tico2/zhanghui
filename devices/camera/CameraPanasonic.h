﻿#ifndef CAMERA_HITACHI1_H
#define CAMERA_HITACHI1_H

#include <QPushButton>
#include "../camera.h"

class CameraPanasonic : public Camera {
    Q_OBJECT
public:
    explicit CameraPanasonic(const QString& name, const QString& host);
    ~CameraPanasonic() override;

    void moveUp() override;
    void moveDown() override;
    void moveLeft() override;
    void moveRight() override;
    void moveHome() override;
    void moveStop() override;

    void zoomWide() override;
    void zoomTele() override;
    void zoomStop() override;

    void prePos(QString action, int pos);
    void setPreset(int pos) override;
    void recallPreset(int pos) override;

private:
    NoneBlockingHttpConnection* connection;
    QString baseUrlRequest;

    void initialize();
    void handleMessage(const QString& msg);
    void executeAction(const QString& action);

    static const QString ZOOM_WIDE;
    static const QString ZOOM_STOP;
    static const QString ZOOM_TELE;

    static const QString MOVE_HOME;
    static const QString MOVE_LEFT;
    static const QString MOVE_RIGHT;
    static const QString MOVE_UP;
    static const QString MOVE_DOWN;
    static const QString MOVE_STOP;

    static const QString PREPOS_SET;
    static const QString PREPOS_RECALL;
    static const QString PREPOS_QUERY;
};

#endif // CAMERA_HITACHI1_H
