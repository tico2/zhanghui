﻿#ifndef TOGGLE_H
#define TOGGLE_H

#include "device.h"
#include <QPushButton>

class Toggle : public Device
{
    Q_OBJECT
public:
    explicit Toggle(const QString& deviceName, QPushButton* btn);
    virtual void toggle(bool checked) = 0;
    void setChecked(bool checked);
    bool isChecked();

private:
    QPushButton* btn;
};

#endif // TOGGLE_H
