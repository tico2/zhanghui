﻿#ifndef CESTRON_H
#define CESTRON_H

#include <QObject>
#include <QGridLayout>
#include <QtWidgets/QPushButton>
#include <QPushButton>
#include <QSlider>
#include "device.h"

class CrestronModule;

class Crestron : public Device {
    Q_OBJECT
public:
    explicit Crestron(const QString& moduleName, const QString& hostname, unsigned short port);
    ~Crestron() override;
    void connectToHost();
    void addModule(CrestronModule* module);
    CrestronModule* getModule(const QString& moduleName);
    void send(const QString& msg);
private:
    QMap<QString, QSet<CrestronModule*>*> prefixModuleSetMap;
    QMap<QString, CrestronModule*> nameModuleMap;
    TcpConnection* connection = nullptr;

    void handleMessage(const QString& msg);
};

class CrestronModule {
public:
    explicit CrestronModule(const QString& name, const QString& prefix);
    virtual ~CrestronModule();
    void setParent(Crestron* crestron);

    const QString name;
    const QString prefix;
protected:
    Crestron* crestron = nullptr;

    virtual void handleMessage(const QString& msg) = 0;
    void executeCommand(const QString& command);
    friend class Crestron;
};
#endif // CESTRON_H
